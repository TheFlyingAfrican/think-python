
def squareRoot(a, precision = 0.001):
    x = 3.0
    iterations = 0
    while True:
        a = float(a)
        y = (x + a/x) / 2
        if (abs(y-x) < precision):
            break
        x = y
        iterations += 1
    print (iterations)
    return y 

# print (squareRoot(4, 0.000000000001))