# from math import pi, factorial
import math
def estimate_pi():
    precision = 1e-15
    terminate = False
    pi_estimate = 0 
    k = 0
    while not terminate:
        sumTerm = (math.factorial(4*k)*(1103 + 26390*k)) / (math.factorial(k)**4 * 396**(4*k))
        if sumTerm < precision:
            terminate = True
        else:
            k += 1
            pi_estimate += sumTerm
     
    pi_estimate = ((pi_estimate * (2 * 2**0.5) / float(9801)) ** -1)
    return pi_estimate

print (estimate_pi())
print (math.pi)
print (math.__dict__)

      
        
