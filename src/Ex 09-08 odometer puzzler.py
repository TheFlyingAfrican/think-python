def is_palindrome(string):
    reverse = string[::-1]
    return string == reverse

def find_odo_reading():
    odo = 100000
    while True and odo <= 999999:
        if is_palindrome(str(odo)[2:]):
            odo += 1
            if is_palindrome(str(odo)[1:]):
                odo += 1
                if is_palindrome(str(odo)[1:5]):
                    return odo-2
        else:
            odo += 1
    
    
print (find_odo_reading())

# odo = 123456
# print (str(odo)[2:])
# print (str(odo)[1:])
# print (str(odo)[1:5])
