def nested_sum(data):
    flatList = []
    for item in data:
        if isinstance(item, list):
            for element in nested_sum(item):
                flatList.append(element)
        else:
            flatList.append(item)
    return flatList
            
print (nested_sum([1,2,3,[3,4,5,[6,7,6]],23,12,141,[2,5,7]]))