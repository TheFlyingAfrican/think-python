def is_sorted(inputList):

    for i in range(0, len(inputList)-1):
        if inputList[i] <= inputList[i+1]:
            i += 1
        else:
            return False
    return True

tempList1 = [1,3,2,3,4,5,6,7]
tempList2 = ['a','b','f','z','c']
print (is_sorted(tempList1))
print (is_sorted(tempList2))