
def is_anagram(word1, word2):
    word1List = []
    word2List= []
    if len(word1) != len(word2):
        return False
    else:
        for i in range(0, len(word1)):
            word1List.append(word1[i])
            word2List.append(word2[i])
        word1List.sort()
        word2List.sort()
        return word1List == word2List
    
print (is_anagram('hello','loleh'))