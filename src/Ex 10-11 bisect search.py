
def bisect_search(sortedList, searchItem):
    if len(sortedList) == 1:
        return searchItem == sortedList[0]
    else:
        splitPoint = len(sortedList)//2
        if sortedList[splitPoint] == searchItem:
            return True
        elif searchItem > sortedList[splitPoint]:
            return bisect_search(sortedList[splitPoint+1:], searchItem)
        else:
            return bisect_search(sortedList[0:splitPoint], searchItem)


thisList = [1,2,3,4,5,6,7,8,9,10]    
print (bisect_search(thisList, 1))