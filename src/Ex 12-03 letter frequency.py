import sys
import string

def most_frequent(stringData):
    
    freqDict = dict()
    for letter in stringData:
        if letter not in (string.punctuation and ' '):
            freqDict[letter] = freqDict.get(letter,0) + 1
#             if letter in freqDict:
#                 freqDict[letter] += 1
#             else:
#                 freqDict[letter] = 1
    sortList = []
    for item, value in freqDict.items():
        sortList.append((value, item))
    sortList.sort(reverse = True)
    for item, value in sortList:
        print (value, item)
    
most_frequent("hello world this is a testaaaaaaaaaaaaaaaaa")