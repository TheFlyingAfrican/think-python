def more_anagrams():
    fin = open('./src/words.txt','r')
    wordDict = dict()
    
    for row in fin:
        sortList = list()
        for letter in row.strip():
            sortList.append(letter)
        sortList.sort()
        sortString = ''
        for item in sortList:
            sortString += item
        if sortString in wordDict:
            wordDict[sortString].append(row.strip())
        else:
            wordDict[sortString] = [row.strip()]
    sortList = []
    for item in wordDict:
        if len(wordDict[item]) > 1:
            #print (wordDict[item])
            sortList.append((len(wordDict[item]), wordDict[item]))
    sortList.sort(reverse=True)
    for count, data in sortList:
       print (data)
    return sortList
            

if __name__ == "__main__":
    more_anagrams()
