import string

def process_file(filename):
    fin = open(filename, 'r')
    
    for row in fin:
        for p in string.punctuation:
            row = row.replace(p, ' ')
        for word in row.strip().split():
            print (word.lower())
process_file('./src/prideandprejudice.txt')   