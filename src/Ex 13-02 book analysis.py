import string

def process_file(filename):
    fin = open(filename, 'r')
    wordDict = dict()
    
    for row in fin:
        for p in string.punctuation:
            row = row.replace(p, ' ')
        for word in row.strip().split():
            wordDict[word] = wordDict.get(word,0) + 1
    wordList = list()
    for word in wordDict:
        wordList.append([word, wordDict[word]])
    wordList.sort(key = lambda word: word[1], reverse = True)
    
    return wordList
    
wordList = process_file('prideandprejudice.txt')
for word, freq in wordList[0:20]:
    print (word, '\t', freq)

  