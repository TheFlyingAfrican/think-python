import string
import random
import bisect

def calc_cumulative(wordList):
    cumSumList = list()
    total = 0
    for item in wordList:
        total += item[1]
        cumSumList.append(total)
    return cumSumList
    
def process_file(filename):
    fin = open(filename, 'r')
    wordDict = dict()
    
    for row in fin:
        for p in string.punctuation:
            row = row.replace(p, ' ')
        for word in row.strip().split():
            wordDict[word] = wordDict.get(word,0) + 1
    wordList = list()
    for word in wordDict:
        wordList.append([word, wordDict[word]])
    wordList.sort(key = lambda word: word[1], reverse = False)
    
    return wordList

    
if __name__ == '__main__':
    word_list = process_file('prideandprejudice.txt')
    cumSum = calc_cumulative(word_list)
    i = random.randint(1, cumSum[len(cumSum)-1])
    print (word_list[bisect.bisect_left(cumSum, i)])
    