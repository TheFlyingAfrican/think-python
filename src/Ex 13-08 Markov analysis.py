import string
import bisect
import random

def skip_gutenberg_header(fp):
    """Reads from fp until it finds the line that ends the header.

    fp: open file object
    """
    for line in fp:
        if line.startswith('*** START OF THIS PROJECT GUTENBERG EBOOK'):
            break
        #else:
            #print (line)

def read_file(filename):
    fin = open(filename, 'r')
    skip_gutenberg_header(fin)
    book_word_list = list()
    for line in fin:
        if line.startswith('*** END OF THIS PROJECT GUTENBERG EBOOK'):
            break
        else:
            for word in clean_row(line).split():
                book_word_list.append(word)
    return book_word_list

def clean_row(line):
    clean_line = ''
    for i in range(0,len(line)):
        if line[i] not in string.punctuation:
            clean_line += line[i]
    return clean_line.lower()

def analyze_book(filename, pfx_len):
    book_words = read_file(filename)
    prefix_suffix_map = dict()
    prefix = ' '.join(book_words[0:pfx_len]) #first prefix
    for word in book_words[pfx_len:]:
        if prefix.strip() in prefix_suffix_map:
            if word in prefix_suffix_map[prefix.strip()]:
                pass
            else:
                prefix_suffix_map[prefix.strip()].append(word)
        else:
            prefix_suffix_map[prefix.strip()] = list([word])
        prefix = ' '.join((prefix.split()[1:])) + ' ' + word
        
    return prefix_suffix_map
    
def main(*args):
    p, l= args
    
    psm_book1 = analyze_book('prideandprejudice.txt', l)
    psm_book2 = analyze_book('sherlockholmes.txt', l)
    
    for i in range(p):
        if i % 2 == 0:
            active_book = psm_book1
        else:
            active_book = psm_book2
        k = random.choice(active_book.keys())
        print ' '.join([k, random.choice(active_book[k])]), 
        
if __name__ == '__main__':
    main(1000, 5)
         

         
