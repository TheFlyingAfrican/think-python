import string
import os

def sed(pattern, replacement, f1, f2):
    try:
        os.chdir('./src')
        read_file = open(f1, 'r')
        write_file = open(f2, 'w')
        
        for line in read_file:
            write_file.write(line.replace(pattern, replacement))
        #write_file.close()
    except:
        print ("unable to read or write to file")

def main(*args):
    sed('the', 'goodbye', 'prideandprejudice.txt', 'goodbyeprejudice.txt')
        
if __name__ == "__main__":
    main()
            