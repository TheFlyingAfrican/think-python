# !/usr/bin/python

import os
import exifread
import time
#import string


###
# 1. get all files and directories from cwd down if recursive = True
# 2. if 
#
#
#
def get_file_list(recursive = False, root=os.getcwd()): 
    """Returns unique list of absolute file pathnames.
    
    Parameters allow recursion from the root directory (defaults to False), and the root path defaults to the current working directory.
    """
    abs_file_list = list()
    if recursive:
        for root, dirs, files in os.walk(root, topdown=False):
            for name in files:
                abs_file_list.append(os.path.join(root, name))
    else:
        [abs_file_list.append(os.path.join(root, x)) for x in os.listdir(root) if os.path.isfile(os.path.join(root,x))]
    return list(set(abs_file_list))
    
def get_exif_tag(file_list): #appends exif data if available
    data = list()
    return data

def print_stat(st): #from http://effbot.org/librarybook/os.htm
    mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime = st
    print ("- size:", size, "bytes")
    print ("- owner:", uid, gid)
    print ("- created:", time.ctime(ctime))
    print ("- last accessed:", time.ctime(atime))
    print ("- last modified:", time.ctime(mtime))
    print ("- mode:", oct(mode))
    print ("- inode/dev:", ino, dev)
    
def get_file_attributes(file_list): #returns list of relevant file attributes
    attribute_dict = dict() 
    #structure of absolute filename as key; list of values of size, create epoch time, create period, modify epoch time, modify period, dict of exif data (empty if no exif data) 
    
    for filename in file_list:
        mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime = os.stat(filename)
        f = open(filename, 'rb')
        exifTags =  exifread.process_file(f)
        f.close()
        attribute_dict[filename] = [size, ctime, year_month(ctime), mtime, year_month(mtime), reduce_exif_tags(exifTags)]
        #attribute_dict[filename] = [size, ctime, year_month(ctime), mtime, year_month(mtime), exifTags]
    return attribute_dict

def reduce_exif_tags(exifTags):
    """Searches for all exif tags with 'Time' or 'Date' and returns dict of all date/time keys and associated values"""
    exifDict = dict()
    for key in exifTags:
        if key.lower().find('datetime') != -1:
            exifDict[key] = exifTags[key]
    return exifDict            
        
    
        
def year_month(inputDate): #returns string of year-month
    timeTuple = time.localtime(inputDate)
    month = "-"
    if timeTuple[1] < 10:
        month += "0"
    return str(timeTuple[0]) + month + str(timeTuple[1])

def main():
    filenames = get_file_list(True, root = "D:\\photos\\zzzz")
    file_dict = get_file_attributes(filenames)
    for value in file_dict:
        print (value, file_dict[value])
        
if __name__ == "__main__":
    main()

